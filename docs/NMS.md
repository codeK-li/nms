# 在 Ubuntu 18.04 上安装 NMS

1. 更新本地软件仓库目录

```shell
apt update
```

2. Install ffmpeg-4

```shell
apt-get install software-properties-common build-essential
add-apt-repository ppa:jonathonf/ffmpeg-4
apt-get ffmpeg
```

查看 ffmpet 版本

```shell
ffmpeg -version
```

**注意：ffmpeg 的版本必须大于 4.0**

3. 安装 Git

```shell
apt install git
```

查看 Git 版本

```shell
git --version
```

4. 安装 curl

```shell
apt install curl
```

5. 安装 Nodejs

```shell
curl -fsSL https://deb.nodesource.com/setup_17.x | sudo -E bash -
sudo apt-get install -y nodejs
```

查看 node && npm 版本

```shell
node -v
npm -v
```

# 下载并运行 NMS

## 下载源代码

```shell
# 进入安装目录
cd /home
```

```shell
# 下载源代码
git clone https://gitlab.com/codeK-li/nms.git
# 进入源代码目录
cd nsm
# 下载源代码的依赖库
npm install
# 编译源代码
npm run build
# 启动 Node Media Server
npm run start
```

# 在 Docker 中安装 NMS

```shell
docker pull ubuntu:20.04

docker run --name nms1 -it -p 1935:1935 -p 8000:8000 ubuntu:20.04 

apt-get update

apt-get -y --no-install-recommends \
 install git curl sudo build-essential ffmpeg

curl -fsSL https://deb.nodesource.com/setup_17.x | sudo -E bash -
sudo apt-get install -y nodejs

cd /home
git clone https://gitlab.com/codeK-li/nms.git
cd nms

npm install
npm run build
npm run start
```

# 推流测试

## 推流

```shell
ffmpeg -re -i demo.flv -c copy -f flv rtmp://localhost/live/XXXX

ffmpeg -re -stream_loop -1 -i demo.flv -c copy -f flv \
  rtmp://localhost/live/XXXX
```

其中:

- localhost 可以换成服务器的 ip
- XXXX 是推流的频道

## 观看

- rtmp 的默认端口为 1935
- ffplay rtmp://localhost/live/XXXX
- ffplay http://localhost:8000/live/XXXX.flv
- ffplay http://localhost:8000/live/XXXX/index.m3u8
