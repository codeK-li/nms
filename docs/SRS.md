# 在 Docker 中安装 SRS

```shell
docker run -p 1935:1935 -p 1985:1985 -p 80:8080 \
  --name srs1 ossrs/srs:3
```

# 推流

 ```shell
ffmpeg -re -i demo.flv -c copy -f flv \
  rtmp://localhost/live/livestream
  
ffmpeg -re -stream_loop -1 -i demo.flv -c copy -f flv \
  rtmp://localhost/live/livestream
```

其中:

- localhost 可以换成服务器的 ip
- livestream 是推流的频道

# 参考

- https://ossrs.net/
- https://hub.docker.com/r/ossrs/srs
- 