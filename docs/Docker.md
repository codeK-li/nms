# 安装 Docker

## Set up the repository

```shell
# 1. Update the apt package index and install packages to allow apt to use a repository over HTTPS:
apt-get update
apt-get install ca-certificates curl gnupg lsb-release

# 2. Add Docker’s official GPG key
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | \
  sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

# 3. Use the following command to set up the stable repository. 
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

## Install Docker Engine

```shell
# 1. Update the apt package index, and install the latest version of Docker Engine and containerd, or go to the next step to install a specific version
apt-get update
apt-get install docker-ce docker-ce-cli containerd.io

# 2. Verify that Docker Engine is installed correctly by running the hello-world image.
docker run hello-world
```

# 在 Docker 中安装 Portainer

```shell
docker volume create portainer_data
docker run -d -p 9443:9443 --name portainer \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer-ce:latest
```